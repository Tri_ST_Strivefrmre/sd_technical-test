## TO Install

### run command : pip install -r requirements.txt 


### Available routes - 

#### base_url: http://localhost:8000/api/v1

##### Child Routes: 

##### 1. Admin - /admin

##### 2. All profiles - /profiles
##### HTTP_METHOD_ALLOWED: GET

##### 3. Search By Id Route - /?id=<UUID>
##### HTTP_METHOD_ALLOWED: GET

##### 4. Search By email - /?email_id
##### HTTP_METHOD_ALLOWED: GET
