from django.urls import path, include
from . import views
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns

router = routers.DefaultRouter()

# route for admin to add candidates
router.register('admin', views.CandidateView, basename='admin_page' )

#routes for public read-only API
router.register('profiles', views.CandidateListView, basename='all_profiles')
router.register(r'profile', views.CandidateListView, basename='profiledetails')



# add routes should start with api/v1/
urlpatterns = [
    path('api/v1/', include(router.urls))
]