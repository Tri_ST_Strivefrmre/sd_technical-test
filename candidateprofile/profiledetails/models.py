from django.db import models
import base64
from uuid import uuid4

# Create your models here.

FAVOURITE_LANGUAGES = [
    ('Python', 'Python'),
    ('PHP', 'PHP'),
    ('Perl', 'Perl'),
    ('Prolog', 'Prolog'),
    ('Fortran', 'Fortran'),
    ('Haskell', 'Haskell')
]

class CandidateProfile(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    real_name = models.CharField(max_length=50, blank=False)
    email_id = models.EmailField(max_length=254, unique=True, blank=False)
    dob = models.DateField(auto_now=False, auto_now_add=False, blank=False)
    australian_citizenship = models.BooleanField(default=False)
    favourite_language = models.CharField(max_length=10, choices=FAVOURITE_LANGUAGES, default='Python')
    description = models.TextField(max_length=500)
    years_as_sw_dev = models.IntegerField(blank=False)
    photo = models.ImageField(upload_to='uploads/photos/', blank=True)
    resume = models.FileField(upload_to='uploads/resumes/', null=False, blank=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_timestamp = models.DateTimeField(auto_now=True, auto_now_add=False)

    def __str__(self):
        return self.real_name

    class Meta:
        ordering = ["-timestamp"]

