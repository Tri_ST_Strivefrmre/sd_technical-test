from django.shortcuts import render
from .models import CandidateProfile
from .serializer import CandidateProfileSerializer, CandidateListSerializer
from django.http import Http404, HttpResponseForbidden

# rest_framework imports
from rest_framework import viewsets
from rest_framework.decorators import api_view, permission_classes
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAdminUser, IsAuthenticated
from rest_framework.pagination import PageNumberPagination
from rest_framework.generics import ListAPIView

# 3rd party django_filter import
from django_filters import rest_framework as filters


# Create your views here.



'''
Function based views

@api_view(['GET'])
@permission_classes([IsAuthenticatedOrReadOnly])
def candidate_list(request):
    if request.method == 'GET':
        all_candidates = CandidateProfile.objects.all()
        serializer = CandidateProfileSerializer(all_candidates, many=True)
        return Response(serializer.data)

@api_view(['GET'])
def candidate_by_id(request, slug):
    try:
        candidate = CandidateProfile.objects.get(slug=slug)
    except:
        return Response(status=status.HTTP_404_NOT_FOUND) '''


# creating a class based ModelViewSet for the admin to add candidate profiles
class CandidateView(viewsets.ModelViewSet):
    queryset = CandidateProfile.objects.all()
    serializer_class = CandidateProfileSerializer
    permission_classes = [IsAuthenticated]


# Using a ReadOnlyModelViewSet so allow for a read only API
class CandidateListView(viewsets.ReadOnlyModelViewSet):
    queryset = CandidateProfile.objects.all()
    serializer_class = CandidateListSerializer
    pagination_class = PageNumberPagination
    http_method_names = ['get'] # only permitting get method for the api
    filter_fields = ('id', 'email_id') # filter candidates by id and email fields

    def get_object(self, id):
        try:
            return CandidateView.object.get(pk=id)
        except CandidateProfile.DoesNotExist:
            raise Http404
    

    def get(self, request):
        if request.method =='GET':
            #id = self.kwargs.get('id')
            candidate = self.get_object(request.id)
            serializer = CandidateProfileSerializer(candidate)
            return Response(serializer.data)
    
    def post(self):
        return HttpResponseForbidden('Request method Forbidden')
    
    def put(self, request):
        pass
    
    def delete(self, request):
        pass
