from rest_framework import serializers
from .models import CandidateProfile
import base64
import hashlib
import mimetypes

# Serializer for the admin
class CandidateProfileSerializer(serializers.ModelSerializer):
    
    resume = serializers.FileField(max_length=None, use_url = True)
    photo = serializers.ImageField(use_url = False)

    
    class Meta:
        model = CandidateProfile
        fields = ['id', 'real_name', 'email_id', 'dob', 'australian_citizenship',
        'favourite_language', 'description', 'years_as_sw_dev', 'photo', 
        'resume' ]


# Serializer for CandidateListView
class CandidateListSerializer(serializers.ModelSerializer):
    resume_base64 = serializers.SerializerMethodField('get_resume_base64')

    resume_sha256 = serializers.SerializerMethodField('get_resume_hashed')

    resume_content_type = serializers.SerializerMethodField('file_content_type')

    
    # function to return base64 encode of file
    def get_resume_base64(self, obj):
        try:
            with open(obj.resume.path, 'rb') as user_file:
                resume_b64 = base64.b64encode(user_file.read())
                return resume_b64.decode('utf-8')
        except Exception as e:
            return str(e)
    
    # function to return file meta-data
    def file_content_type(self, obj):
        return str(mimetypes.MimeTypes().guess_type(obj.resume.path)[0])
            
    # function to return hashed file data
    def get_resume_hashed(self, obj):
        try:
            with open(obj.resume.path, 'rb') as user_file:
                resume_hash = hashlib.sha256(user_file.read())
                return resume_hash.hexdigest()
        except Exception as e:
            return str(e)

    class Meta:
        model = CandidateProfile
        fields = ['id', 'real_name', 'email_id', 'dob', 'australian_citizenship',
        'favourite_language', 'description', 'years_as_sw_dev', 'photo', 
        'resume_base64', 'resume_sha256', 'resume_content_type']